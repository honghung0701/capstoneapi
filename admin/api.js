const BASE_URL = "https://633ec05d0dbc3309f3bc54a6.mockapi.io"

function getProductList() {
    return axios.get(`${BASE_URL}/QLSP`)
        .then(function (res) {
            return res.data
        })
        .catch(function (err) {
            console.log('err: ', err);
            return []
        })
};

function postProductList(data) {
    return axios.post(`${BASE_URL}/QLSP`, data)
        .then(function (res) {
            return res.data
        })
        .catch(function (err) {
            console.log('err: ', err);
            return []
        })
};

function delProductList(id) {
    return axios.delete(`${BASE_URL}/QLSP/${id}`)
        .then(function (res) {
            return res.data
        })
        .catch(function (err) {
            console.log('err: ', err);
            return []
        })
}

// update truyền 2 tham số (id, data)
function updateProductList(id, data) {
    return axios.put(`${BASE_URL}/QLSP/${id}`, data)
        .then(function (res) {
            return res.data
        })
        .catch(function (err) {
            console.log('err: ', err);
            return []
        })
}