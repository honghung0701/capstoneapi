
function render() {
    getProductList().then(data => {
        renderProductList(data);
        console.log('data: ', data);

    });
};


function saveProduct() {
    var data = getInfor();
    const valid = validateData(data);
    if (!valid) return;
    if (data.id) {
        updateProductList(data.id, data)
            .then(function (res) {
                render();
            });
    } else {
        postProductList(data)
            .then(function (res) {
                render();
            });
    }
    closeModal();

};

function delProduct(id) {
    console.log(id);
    delProductList(id)
        .then(function (res) {
            render();
        });
};

function openUpdateProduct(data) {
    loadDataToForm(data);

};

function validateData(list) {
    const validName = list.validateName();
    const validPrice = list.validatePrice();
    const validScreen = list.validateScreen();
    const validBackCamera = list.validateBackCamera();
    const validFrontCamera = list.validateFrontCamera();
    const validImg = list.validateImg();
    const validDesc = list.validateDesc();
    const validType = list.validateType();
    return validName && validPrice && validScreen && validBackCamera && validFrontCamera && validImg && validDesc && validType;
};






