function getInfor() {
    let id = document.getElementById("id").value.trim();
    let name = document.getElementById("name").value.trim();
    let price = document.getElementById("price").value.trim();
    let screen = document.getElementById("screen").value.trim();
    let backCamera = document.getElementById("backCamera").value.trim();
    let frontCamera = document.getElementById("frontCamera").value.trim();
    let img = document.getElementById("img").value.trim();
    let desc = document.getElementById("desc").value.trim();
    let type = document.getElementById("type").value.trim();
    console.log(type);

    // trả thông tin từ form về obj
    return new Product(
        id || undefined,
        name,
        price,
        screen,
        backCamera,
        frontCamera,
        img,
        desc,
        type
    )
};

function renderProductList(data) {
    let contentHTML = "";
    data.forEach(function (item) {
        let dataObj = new Product(
            item.id,
            item.name,
            item.price,
            item.screen,
            item.backCamera,
            item.frontCamera,
            item.img,
            item.desc,
            item.type
        )
        const product = JSON.stringify(dataObj);
        console.log(product);
        contentHTML += `<tr>
            <td>${dataObj.id}</td>
            <td>${dataObj.name}</td>
            <td>${numberFormat(dataObj.price)}</td>
            <td>${dataObj.screen}</td>
            <td>${dataObj.backCamera}</td>
            <td>${dataObj.frontCamera}</td>
            <td><img width="50px" src="${dataObj.img}" alt="" />
            </td>
            <td>${dataObj.desc}</td>
            <td>${dataObj.type}</td>
            <td>
                <button class="btn btn-danger" onclick="delProduct(${dataObj.id})">Xóa</button>
                <button class="btn btn-info" onclick='openUpdateProduct(${product})'>Sửa</button>
            </td>
        </tr>`
    })
    document.getElementById("tblDanhSachSP").innerHTML = contentHTML;
};
function loadDataToForm(data) {
    $('#myModal').modal('show');
    document.getElementById("id").value = data.id;
    document.getElementById("name").value = data.name;
    document.getElementById("price").value = data.price;
    document.getElementById("screen").value = data.screen;
    document.getElementById("backCamera").value = data.backCamera;
    document.getElementById("frontCamera").value = data.frontCamera;
    document.getElementById("img").value = data.img;
    document.getElementById("desc").value = data.desc;
    document.getElementById("type").value = data.type;

};


function closeModal() {
    document.getElementById("id").value = "";
    document.getElementById("name").value = "";
    document.getElementById("price").value = "";
    document.getElementById("screen").value = "";
    document.getElementById("backCamera").value = "";
    document.getElementById("frontCamera").value = "";
    document.getElementById("img").value = "";
    document.getElementById("desc").value = "";
    document.getElementById("type").value = "";

    $('#myModal').modal('hide');
}

function resetSpan() {
    document.getElementById("valid-name").innerText = "";
    document.getElementById("valid-price").innerText = "";
    document.getElementById("valid-screen").innerText = "";
    document.getElementById("valid-backCamera").innerText = "";
    document.getElementById("valid-frontCamera").innerText = "";
    document.getElementById("valid-img").innerText = "";
    document.getElementById("valid-desc").innerText = "";
    document.getElementById("valid-type").innerText = "";
}
function closeForm() {
    resetSpan();
}
function numberFormat(number) {
    return `${new Intl.NumberFormat("de-DE").format(number)} VND`;
};
