const BASE_URL = "https://633ec05d0dbc3309f3bc54a6.mockapi.io";

// R (read)
function loadProductFromAPI() {
  return axios.get(`${BASE_URL}/QLSP`)
    .then(function (res) {
      return res.data;
    })
    .catch(function (err) {
      console.log("err: ", err);
      return [];
    });
}
