function render() {
  getProductList().then((data) => {
    renderProductList(data);
  });
}
function renderProductList(data) {
  let contentHTML = "";
  data.forEach(function (item) {
    let dataObj = new Product(
      item.id,
      item.name,
      item.price,
      item.screen,
      item.backCamera,
      item.frontCamera,
      item.img,
      item.desc,
      item.type
    );

    contentHTML += `<div class="card">
    <div class="top-bar">
        
        <em class="stocks">In Stock</em>
    </div>
    <div class="img-container">
        <img class="product-img" src="${dataObj.img}" alt="">
        <div class="out-of-stock-cover"><span>Out Of Stock</span></div>
    </div>
    <div class="details">
        <div class="name-fav">
            <strong class="product-name">${dataObj.name}</strong>
            <button onclick="this.classList.toggle(&quot;fav&quot;)" class="heart"><i class="fas fa-heart"></i></button>
        </div>
        <div class="wrapper">
            <h5>Camera trước: ${dataObj.frontCamera}</h5>
            <h5>Camera sau: ${dataObj.backCamera}</h5>
            <p>Mô tả: ${dataObj.desc}</p>
        </div>
        <div class="purchase">
            <p class="product-price">${numberFormat(dataObj.price)}</p>
            <span class="btn-add">
                <div>
                    <button onclick='addToCart(${JSON.stringify(
                      dataObj
                    )})' class="add-btn">Add <i class="fas fa-chevron-right"></i></button>
                </div>
            </span>
        </div>
    </div>
</div>`;
  });
  document.getElementById("inforProducts").innerHTML = contentHTML;
}
function limitPurchase() {
  showSideNav();
  document.getElementById("cover purchase-cover").style.display = "none";
  document.getElementById("stock-limit").style.display = "none";
}

function numberFormat(number) {
  return `${new Intl.NumberFormat("de-DE").format(number)} VND`;
}


