function Product(
    _id,
    _name,
    _price,
    _screen,
    _backCamera,
    _frontCamera,
    _img,
    _desc,
    _type
) {
    this.id = _id;
    this.name = _name;
    this.price = _price;
    this.screen = _screen;
    this.backCamera = _backCamera;
    this.frontCamera = _frontCamera;
    this.img = _img;
    this.desc = _desc;
    this.type = _type;

    // validate
    this.validateName = function () {
        let errorMsg = "";
        if (!this.name) {
            errorMsg = "không để trống";
        }
        document.getElementById("valid-name").innerHTML = errorMsg;
        return errorMsg == "";
    };

    this.validatePrice = function () {
        let errorMsg = "";
        if (this.price) {
            const pattern = /^[0-9]{0,}$/;
            let isPrice = pattern.test(this.price);
            if (!isPrice) {
                errorMsg = "chỉ nhập số"
            };
        } else {
            errorMsg = "không để trống"
        }
        document.getElementById("valid-price").innerHTML = errorMsg;
        return errorMsg == "";
    };

    this.validateScreen = function () {
        let errorMsg = "";
        if (!this.screen) {
            errorMsg = "không để trống";
        }
        document.getElementById("valid-screen").innerHTML = errorMsg;
        return errorMsg == "";
    };

    this.validateBackCamera = function () {
        let errorMsg = "";
        if (!this.backCamera) {
            errorMsg = "không để trống";
        }
        document.getElementById("valid-backCamera").innerHTML = errorMsg;
        return errorMsg == "";
    };

    this.validateFrontCamera = function () {
        let errorMsg = "";
        if (!this.frontCamera) {
            errorMsg = "không để trống";
        }
        document.getElementById("valid-frontCamera").innerHTML = errorMsg;
        return errorMsg == "";
    };

    this.validateImg = function () {
        let errorMsg = "";
        if (!this.img) {
            errorMsg = "không để trống";
        }
        document.getElementById("valid-img").innerHTML = errorMsg;
        return errorMsg == "";
    };

    this.validateDesc = function () {
        let errorMsg = "";
        if (!this.desc) {
            errorMsg = "không để trống";
        }
        document.getElementById("valid-desc").innerHTML = errorMsg;
        return errorMsg == "";
    };

    this.validateType = function () {
        let errorMsg = "";
        if (!this.type) {
            errorMsg = "không để trống";
        }
        document.getElementById("valid-type").innerHTML = errorMsg;
        return errorMsg == "";
    };
};



